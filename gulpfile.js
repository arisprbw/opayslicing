const {dest, src, series, watch, parallel}   =   require('gulp');
      gulpSass      =   require('gulp-sass');
      gulpPostcss   =   require('gulp-postcss');
      gulpRename    =   require('gulp-rename');
      autoprefixer  =   require('autoprefixer');
      browserSync   =   require('browser-sync').create();
      cssnano       =   require('cssnano');
      gulpUglify    =   require('gulp-uglify');
      pipeline      =   require('readable-stream').pipeline;

const files = {
    cssPath    : {
        src    : 'public/src/scss/**/*.scss',
        des     : 'public/dist/css',
    },
    jsPath      : {
        src     : 'public/src/js/**/*.js',
        des     : 'public/dist/js'
    },
    awesome     : {
        css         : 'node_modules/font-awesome/css/font-awesome.min.css',
        font        : 'node_modules/font-awesome/fonts/*',
        destCss     : 'public/dist/vendor/fontawesome/css',
        destFont    : 'public/dist/vendor/fontawesome/fonts'
    },
    bootstrap   : {
        css     : 'node_modules/bootstrap/dist/css/bootstrap.min.css',
        js      : 'node_modules/bootstrap/dist/js/bootstrap.min.js',
        dest    : 'public/dist/vendor/bootstrap'
    },
    syncPath    : 'public/**/*.html',
}

function cssCompile(){
    return src(files.cssPath.src)
    .pipe(gulpSass().on('error', gulpSass.logError ))
    .pipe(gulpPostcss([autoprefixer(), cssnano()]))
    .pipe(gulpRename({suffix : '.min'}))
    .pipe(dest(files.cssPath.des))
    .pipe(browserSync.stream())
}

function jsCompile(){
    return src(files.jsPath.src)
    .pipe(gulpUglify())
    .pipe(gulpRename({suffix : '.min'}))
    .pipe(dest(files.jsPath.des))
    .pipe(browserSync.stream())
}

function importVendor(){
    // Font Awesome
    src(files.awesome.css)
    .pipe(dest(files.awesome.destCss))
    src(files.awesome.font)
    .pipe(dest(files.awesome.destFont))

    //Bootstrap
    src([files.bootstrap.css, files.bootstrap.js])
    .pipe(dest(files.bootstrap.dest))
}

function runCompile(){
    watch(files.cssPath.src, cssCompile);
    watch(files.jsPath.src, jsCompile);
    browserSync.init({
        server : {baseDir : './public'}
    })
    watch(files.syncPath).on('change', browserSync.reload);
}
exports.default = parallel(
    importVendor,
    cssCompile,
    jsCompile,
    runCompile
)